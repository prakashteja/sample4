package com.capgemini.contacts.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.capgemini.contacts.bean.ContactDetails;
import com.capgemini.contacts.exception.ContactsException;
import com.capgemini.contacts.service.ContactsValidator;

public class ContactClient{

	static Scanner sc = new Scanner(System.in);
	
    public static void main(String[] args) throws ContactsException {
		
		int option = 0;
		do{
			System.out.println("Select Options");
			System.out.println("1.Add New Contact");
			System.out.println("2.Delete Contact");
			System.out.println("3.Exit");
			
			
			option = sc.nextInt();
			switch(option)
			{
			case 1: addContact();
					break;
			case 2: deleteContact();
					break;
			case 3: System.exit(0);
			default: System.out.println("Wrong Option");
			
			}
			
		}while(option!=3);
		
	}
	

	
		public static void addContact() throws ContactsException
		{
			
			ContactDetails contact =  new ContactDetails();
		
			ContactsValidator contactValid = new ContactsValidator();
			
			while(true)
			{
				System.out.println("Enter the ContactName: ");
				String cName = sc.next();
				if(contactValid.validateCName(cName))
				{
					contact.setcName(cName);
					break;
				}
				else
				{
					System.err.println("\n Invalid Contact Name");
				}	
			
			}
			while(true)
			{
				System.out.println("Enter the MobileNumber: ");
				String mobileNo = sc.next();
				if(contactValid.validateMobileNo(mobileNo))
				{
					contact.setMobileNo1((mobileNo));
					break;
				}	
				else
				{	
				System.err.println("\n Enter the valid Mobile Number");
				}
			}	
			while(true)
			{	
				System.out.println("Enter the EmailId: ");
				String emailId = sc.next();
				if(contactValid.validateEmailId(emailId))
				{
					contact.setEmailId(emailId);
					break;
				}	
				else
				{
					System.err.println("\n Invalid Email Id");
				}
			}
			while(true)
			{
				System.out.println("Enter the GroupName: ");
				String groupName = sc.next();
				if(contactValid.validateGroupName(groupName))
				{
					contact.setGroupName(groupName);
					break;
				}
				else
				{
					System.err.println("\n Provide valid Group Name");
				}
			}		
		
		}
	

		  
		/*ContactsHelper conthelp=new ContactsHelper();
		while(true){
		
			conthelp.addContact(contact);
			System.out.println("Contact Added");
			break;
		}
	}*/
	
	public static void deleteContact()
	{
		while(true){
			
			ContactDetails contactDetails = new ContactDetails(); 
			
			System.out.println("Enter the ContactName: ");
			
			String cName = sc.next();
			
		}
		
	}
	
	
	}
