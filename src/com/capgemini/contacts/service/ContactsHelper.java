package com.capgemini.contacts.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.capgemini.contacts.bean.ContactDetails;
import com.capgemini.contacts.exception.ContactsException;

public class ContactsHelper {

	static ArrayList<Object> list1 = new ArrayList<Object>();
	
	static{
	
		ContactDetails c1 = new ContactDetails();
		
		c1.setContactId(1);
		c1.setcName("Kirti Roy");
		c1.setMobileNo1("9234534500");
		c1.setMobileNo2(null);
		c1.setEmailId("kirtiroy@yahoo.co.in");
		c1.setGroupName("Family");
		
		ContactDetails c2 = new ContactDetails();
		
		c2.setContactId(2);
		c2.setcName("Raj Singh");
		c2.setMobileNo1("8288866678");
		c2.setMobileNo2("8234343434");
		c2.setEmailId("Arun16@gmail.com");
		c2.setGroupName("Friends");
		
		list1.add(c1);
		list1.add(c2);
		
	}
	
	public boolean addContact(ContactDetails contact)throws ContactsException{
		
		
/*		for(ContactDetails contact1:list1){
			
			if(contact1.getcName()==contact.getcName()){
				
				throw new ContactsException("Duplicate Contact.Failed to add the Contact", null);
			}
			
			
		}*/
		return list1.add(contact);
		
	}
		
}
