package com.capgemini.contacts.service;

public class ContactsValidator {

	public boolean validateCName(String cName)
	{	
		return cName.matches("[A-Za-z\\s]{5,15}");
	}
	public boolean validateMobileNo(String mobileNo)
	{
		return mobileNo.matches("^[789]\\d{9}");
	}
	public boolean validateEmailId(String emailId)
	{
		return emailId.matches("^([_a-zA-Z0-9-]+(\\.[_a-zA-Z0-9-]+)*@[a-zA-Z]+(\\.[a-zA-Z]+)*(\\.[a-zA-Z]{1,15}))?$");
	}
	public boolean validateGroupName(String groupName)
	{
		return groupName.matches("\\b(Friends|Family|CoWorkers)\\b");
	}
}
