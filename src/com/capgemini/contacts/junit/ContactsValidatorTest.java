#12345
package com.capgemini.contacts.junit;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.capgemini.contacts.service.ContactsValidator;


public class ContactsValidatorTest { /////
	
	
      ContactsValidator contactValid;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	
	}

	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}

	
	@Before
	public void dosetUp() throws Exception {
		
		contactValid=new ContactsValidator();
		
	}

	@Test
	public void testValidateCName() {
		
		assertTrue(contactValid.validateCName("prakash"));
		
		
	}
	@Test
	public void negativeTestValidateCName(){
		assertFalse(contactValid.validateCName("suryanarayana sastry"));
	}
	@Test
	public void testValidateMobileNo(){
		
		assertTrue(contactValid.validateMobileNo("9247531407"));
		
	}
	@Test
	public void negativeTestValidateMobileNo(){
		
		assertFalse(contactValid.validateMobileNo("6435689760"));
	}
	@Test
	public void testValidateEmailId(){
		
		assertTrue(contactValid.validateEmailId("abc123@gmail.com"));
	}
	@Test
	public void negativeTestValidateEmailId(){
		
		assertFalse(contactValid.validateEmailId("abcgmail.com"));
	}
	@Test
	public void testValidateGroupName(){
		
		assertTrue(contactValid.validateGroupName("Friends"));
	}
	@Test
	public void negativeTestValidateGroupName(){
		
		assertFalse(contactValid.validateGroupName("Workers"));
	}


	
	
}
